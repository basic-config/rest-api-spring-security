package net.suby.project.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.suby.project.user.persistence.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, String> {
}
