package net.suby.project.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.suby.project.user.persistence.Member;
import net.suby.project.user.persistence.MemberRoleId;

@Repository
public interface MemberRoleRepository extends JpaRepository<Member, MemberRoleId> {
}
