package net.suby.project.user.service;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import net.suby.project.user.persistence.Member;
import net.suby.project.user.persistence.MemberRole;
import net.suby.project.user.repository.MemberRepository;

@Service
public class CustomUserService implements UserDetailsService {

    private MemberRepository memberRepository;

    public CustomUserService(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Member> member = this.memberRepository.findById(username);
        List<String> memberRoles = member.get().getMemberRole().stream()
                .map(MemberRole::getUserRole)
                .collect(Collectors.toList());
        List<GrantedAuthority> authorities = buildUserAuthority(memberRoles);
        return buildUserForAuthentication(member.get(), authorities);
    }

    private User buildUserForAuthentication(Member member, List<GrantedAuthority> authorities) {
        return new User(member.getId(), member.getPassword(), true, true, true, true, authorities);
    }

    // 권한 설정
    private List<GrantedAuthority> buildUserAuthority(List<String> userRoles) {
        Set<GrantedAuthority> setAuths = new HashSet<>();

        // Build user's authorities
        for (String userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole));
        }

        List<GrantedAuthority> Result = new ArrayList<>(setAuths);

        return Result;
    }
}
