package net.suby.project.user.persistence;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "member_role")
@Setter
@Getter
@IdClass(MemberRoleId.class)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberRole {
    @Id
    private String id;
    @Id
    private String userRole;
}
