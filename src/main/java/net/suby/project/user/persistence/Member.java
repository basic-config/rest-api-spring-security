package net.suby.project.user.persistence;

import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "member")
@Setter
@Getter
public class Member {
    @Id
    private String id;
    private String password;
    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "id")
    private List<MemberRole> MemberRole;
}
