package net.suby.project.user.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.suby.project.user.dto.UserDto;
import net.suby.project.user.service.CustomUserService;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private CustomUserService userService;

    @PostMapping("/login")
    public UsernamePasswordAuthenticationToken login(@RequestBody UserDto userDto, HttpSession session) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDto.getUsername(), userDto.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetails user = userService.loadUserByUsername(userDto.getUsername());
            return new UsernamePasswordAuthenticationToken(user.getUsername(), userDto.getPassword(), user.getAuthorities());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
